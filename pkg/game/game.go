package game

import (
	"gitlab.com/wise_kaa/test/pkg/repository"
	"gitlab.com/wise_kaa/test/pkg/structs"
)

func GameComplete(request structs.GameCompleteRequest) error {
	return repository.AddAccountRec(request)
}
