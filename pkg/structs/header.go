package structs

import (
	"fmt"
)

type SourceType string

const (
	SourceTypeGame    SourceType = "game"
	SourceTypeServer  SourceType = "server"
	SourceTypePayment SourceType = "payment"
)

func (v SourceType) Validate() (errors []string) {
	if v != SourceTypeGame && v != SourceTypeServer && v != SourceTypePayment {
		errors = append(errors, fmt.Sprintf("invalid SourceType: %s", v))
	}

	return
}

func (v SourceType) String() string {
	return string(v)
}
