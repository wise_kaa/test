package main

import (
	"gitlab.com/wise_kaa/test/configuration"
	"gitlab.com/wise_kaa/test/database"
	"gitlab.com/wise_kaa/test/logs"
	"gitlab.com/wise_kaa/test/pkg/repository"
	"log"
	"time"
)

func init() {
	// config
	if err := configuration.InitConfig(); err != nil {
		log.Fatal(err)
	}

	// logger
	if err := logs.Init(); err != nil {
		log.Fatal(err)
	}

	if err := database.Init(); err != nil {
		log.Fatal(err)
	}
}

func main() {
	for {
		repository.CancelOddRec()
		time.Sleep(configuration.GetConfig().Settings.AccountCancelationInterval * time.Minute)
	}
}
