package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/wise_kaa/test/app"
	"gitlab.com/wise_kaa/test/configuration"
	"gitlab.com/wise_kaa/test/database"
	"gitlab.com/wise_kaa/test/logs"
)

func init() {
	// config
	if err := configuration.InitConfig(); err != nil {
		log.Fatal(err)
	}

	// logger
	if err := logs.Init(); err != nil {
		log.Fatal(err)
	}

	if err := database.Init(); err != nil {
		log.Fatal(err)
	}
}

func main() {
	router := app.Router()

	srv := &http.Server{
		Addr:    fmt.Sprint(configuration.GetConfig().HTTP.Host, ":", configuration.GetConfig().HTTP.Port),
		Handler: router,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Println("Shuting down server...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server forced to shutdown:", err)
	}

	log.Println("Server exiting")
}

func handler(w http.ResponseWriter, r *http.Request) {
	if _, err := w.Write([]byte("OK!")); err != nil {
		log.Fatal(err)
	}
}
