FROM golang:latest AS builder
EXPOSE 8080
RUN mkdir /app
ADD . /app/
WORKDIR /app
RUN go build -o main ./cmd/myservice
#RUN go build -o cron ./cmd/cron

COPY configuration/ /app/configuration

ENTRYPOINT ["/app/main"]

