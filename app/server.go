package app

import (
	"fmt"
	"gitlab.com/wise_kaa/test/pkg/structs"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/wise_kaa/test/app/controller"
)

func Router() http.Handler {
	router := gin.Default()

	router.Use(ProcessHeaders())

	router.GET("/healthcheck", controller.HealthCheck)
	router.POST("/game_complete", controller.GameCompleteHandler)

	return router
}

func ProcessHeaders() gin.HandlerFunc {
	return func(c *gin.Context) {
		sourceType := c.GetHeader("Source-Type")
		if sourceType != structs.SourceTypeGame.String() &&
			sourceType != structs.SourceTypePayment.String() &&
			sourceType != structs.SourceTypeServer.String() {
			controller.ErrorResponseSingle(c, fmt.Errorf("wrong Source-Type header"))
			c.Abort()
			return
		}

	}
}
