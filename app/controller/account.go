package controller

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"gitlab.com/wise_kaa/test/pkg/game"
	"gitlab.com/wise_kaa/test/pkg/structs"
	"io/ioutil"
	"net/http"
)

func GameCompleteHandler(c *gin.Context) {
	var request structs.GameCompleteRequest

	data, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		ErrorResponseSingle(c, err)
		return
	}

	err = json.Unmarshal(data, &request)
	if err != nil {
		ErrorResponseSingle(c, err)
		return
	}

	if validationErrors := request.Validate(); len(validationErrors) > 0 {
		ErrorResponseMulti(c, validationErrors)
		return
	}

	err = game.GameComplete(request)
	if err != nil {
		ErrorResponseSingle(c, err)
		return
	}
	c.String(http.StatusOK, "ok")
}
